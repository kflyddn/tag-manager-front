import Layout from "@/layout/index.vue";


const tagRouter = {
  path: '/tag',
  component: Layout,
  redirect: '/tag/category-list',
  name: 'Tag',
  meta: {
    title: '类别-标签管理',
    icon: 'tree-table',
    roles:'admin'
  },
  children: [
    {
      path: 'category-list',
      component: () => import('@/views/tag/category-list'),
      name: 'CategoryManagement',
      meta: {title: '类别管理', icon: 'tree'}
    },
    {
      path: 'tag-list',
      component: () => import('@/views/tag/tag-list'),
      name: 'TagManagement',
      meta: {title: '标签管理', icon: 'el-icon-price-tag'},
    }
  ]
}

export default tagRouter

import request from '@/utils/request'



export function fetchTagList(query) {
  return request({
    url: '/category-tag/tag-list',
    method: 'post',
    data: query
  })
}

export function fetchItemList(query) {
  return request({
    url: '/tag-item/item-list',
    method: 'post',
    data: query
  })
}

export function addToTag(itemId, tagId) {
  let formData = new FormData()
  formData.append('itemId', itemId)
  formData.append('tagId', tagId)
  return request({
    url: '/tag-item/add-item-to-tag',
    method: 'post',
    data: formData
  })
}


export function findItems(itemName) {
  let formData = new FormData()
  formData.append('itemName', itemName)
  return request({
    url: '/tag-item/find-items-by-name',
    method: 'post',
    data: formData
  })
}

export function removeItemFromTag(itemId, tagId) {
  let formData = new FormData()
  formData.append('itemId', itemId)
  formData.append('tagId', tagId)
  return request({
    url: '/tag-item/remove-item-from-tag',
    method: 'post',
    data: formData
  })
}


export function findCategories(categoryName) {
  let formData = new FormData()
  formData.append('categoryName', categoryName)
  return request({
    url: '/tag-item/find-categories-by-name',
    method: 'post',
    data: formData
  })
}

export function addItemByTagId(itemName, tagId) {
  let formData = new FormData()
  formData.append('itemName', itemName)
  formData.append('tagId', tagId)
  return request({
    url: '/tag-item/add-new-item-by-tag-id',
    method: 'post',
    data: formData
  })
}

export function deleteItemById(itemId) {
  let formData = new FormData()
  formData.append('itemId', itemId)
  return request({
    url: '/tag-item/delete-item-by-id',
    method: 'post',
    data: formData
  })
}

export function addTag(tagName) {
  let formData = new FormData()
  formData.append('tagName', tagName)
  return request({
    url: '/tag-item/add-tag',
    method: 'post',
    data: formData
  })
}

export function findItemsOnlyFollowing(tagId) {
  let formData = new FormData()
  formData.append('tagId', tagId)
  return request({
    url: '/tag-item/find-items-only-following',
    method: 'post',
    data: formData
  })
}

export function deleteTagById(tagId) {
  let formData = new FormData()
  formData.append('tagId', tagId)
  return request({
    url: '/category-tag/delete-tag-by-id',
    method: 'post',
    data: formData
  })
}

export function deleteTagCascade(categoryId) {
  let formData = new FormData()
  formData.append('tagId', categoryId)
  return request({
    url: '/tag-item/delete-tag-cascade-by-id',
    method: 'post',
    data: formData
  })
}



export function updateCommentByItemId(itemId,comment) {
  let formData = new FormData()
  formData.append('itemId', itemId)
  formData.append('comment', comment)
  return request({
    url: '/tag-item/update-comment-by-item-id',
    method: 'post',
    data: formData
  })
}

export function updateCommentByTagId(tagId,comment) {
  let formData = new FormData()
  formData.append('tagId', tagId)
  formData.append('comment', comment)
  return request({
    url: '/category-tag/update-comment-by-tag-id',
    method: 'post',
    data: formData
  })
}

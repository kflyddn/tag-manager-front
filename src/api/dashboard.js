import request from '@/utils/request'


export function fetchCategoryCount() {
  return request({
    url: '/statistics/categories',
    method: 'get',
  })
}

export function fetchTagCount() {
  return request({
    url: '/statistics/tags',
    method: 'get',
  })
}
export function fetchLogCount() {
  return request({
    url: '/statistics/logs',
    method: 'get',
  })
}

export function fetchItemCount() {
  return request({
    url: '/statistics/items',
    method: 'get',
  })
}

export function fetchActivityList() {
  return request({
    url: '/statistics/activities',
    method: 'get',
  })
}

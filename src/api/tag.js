import request from '@/utils/request'
import form from 'element-ui/packages/form'
import ta from 'element-ui/src/locale/lang/ta'

export function fetchCategoryList(query) {
  return request({
    url: '/category-tag/category-list',
    method: 'post',
    data: query
  })
}

export function fetchTagList(query) {
  return request({
    url: '/category-tag/tag-list',
    method: 'post',
    data: query
  })
}

export function removeTagFromCategory(tagId, categoryId) {
  let formData = new FormData()
  formData.append('tagId', tagId)
  formData.append('tagId', categoryId)
  return request({
    url: '/category-tag/remove-tag-from-category',
    method: 'post',
    data: formData
  })
}

export function addToCategory(tagId, categoryId) {
  let formData = new FormData()
  formData.append('tagId', tagId)
  formData.append('tagId', categoryId)
  return request({
    url: '/category-tag/add-tag-to-category',
    method: 'post',
    data: formData
  })
}

export function findTags(tagName) {
  let formData = new FormData()
  formData.append('tagName', tagName)
  return request({
    url: '/category-tag/find-tags-by-name',
    method: 'post',
    data: formData
  })
}

export function findCategories(categoryName) {
  let formData = new FormData()
  formData.append('categoryName', categoryName)
  return request({
    url: '/category-tag/find-categories-by-name',
    method: 'post',
    data: formData
  })
}

export function addTagByCategoryId(tagName, categoryId) {
  let formData = new FormData()
  formData.append('tagName', tagName)
  formData.append('categoryId', categoryId)
  return request({
    url: '/category-tag/add-new-tag-by-category-id',
    method: 'post',
    data: formData
  })
}

export function deleteTagById(tagId) {
  let formData = new FormData()
  formData.append('tagId', tagId)
  return request({
    url: '/category-tag/delete-tag-by-id',
    method: 'post',
    data: formData
  })
}

export function addCategory(categoryName) {
  let formData = new FormData()
  formData.append('categoryName', categoryName)
  return request({
    url: '/category-tag/add-category',
    method: 'post',
    data: formData
  })
}

export function findTagsOnlyFollowing(categoryId) {
  let formData = new FormData()
  formData.append('categoryId', categoryId)
  return request({
    url: '/category-tag/find-tags-only-following',
    method: 'post',
    data: formData
  })
}

export function deleteCategoryCascade(categoryId) {
  let formData = new FormData()
  formData.append('categoryId', categoryId)
  return request({
    url: '/category-tag/delete-category-cascade-by-id',
    method: 'post',
    data: formData
  })
}

export function deleteCategoryById(categoryId) {
  let formData = new FormData()
  formData.append('categoryId', categoryId)
  return request({
    url: '/category-tag/delete-category-by-id',
    method: 'post',
    data: formData
  })
}

export function updateCommentByCategoryId(categoryId,comment) {
  let formData = new FormData()
  formData.append('categoryId', categoryId)
  formData.append('comment', comment)
  return request({
    url: '/category-tag/update-comment-by-category-id',
    method: 'post',
    data: formData
  })
}

export function updateCommentByTagId(tagId,comment) {
  let formData = new FormData()
  formData.append('tagId', tagId)
  formData.append('comment', comment)
  return request({
    url: '/category-tag/update-comment-by-tag-id',
    method: 'post',
    data: formData
  })
}

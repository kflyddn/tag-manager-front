import Layout from "@/layout/index.vue";


const itemRouter = {
  path: '/item',
  component: Layout,
  redirect: '/item/item-list',
  name: 'Item',
  alwaysShow: true,
  meta: {
    title: '标签-实体管理',
    icon: 'tab',
    roles: ['admin']
  },
  children: [
    {
      path: 'tag-list',
      component: () => import('@/views/item/tag-list'),
      name: 'TagList',
      meta: {title: '标签管理', icon: 'el-icon-price-tag'}
    },
    {
      path: 'item-list',
      component: () => import('@/views/item/item-list'),
      name: 'ItemList',
      meta: {title: '实体管理', icon: 'el-icon-box'}
    }

  ]
}

export default itemRouter
